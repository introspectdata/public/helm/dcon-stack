# Documentation

The DCON stack contains the helm charts of four core kubernetes applications:
- Kubernetes Dashboard
- Cert Manager  
- OAuth2 Proxy
- Nginx Ingress


This guide was developed using GKE, but should apply to other Kubernetes installations as well.


## Preparing your cluster
*It is assumed that you already have a* **[Kubernetes cluster](https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-intro/)** *and have your [Kubectl context](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-context-and-configuration) set up*


Before installing the DCON stack, there a few operations you must perform first:
- Install Helm and Tiller:
  - Create the tiller service account: `kubectl -n kube-system apply -f tiller-sa.yaml`
  - Download and run Helm setup for your client: `curl -L https://git.io/get_helm.sh | bash`
  - Initialize helm for client and cluster: `helm init --service-account tiller`
- Set up a DCON namespace:
  - `kubectl create namespace core`
  - `kubectl label namespace core certmanager.k8s.io/disable-validation=true`
- Install prerequisites for Cert Manager: `kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-XX/deploy/manifests/00-crds.yaml`
- Create the secrets for your dns service account


## Configuring your values file
The umbrella values contains the values info for all four apps.

### Kubernetes Dashboard
The Kubernetes dashboard is mostly 'ready to go' but you need to update the host names with your domain, and make sure it is using the correct Nginx Ingress class.

### Cert Manager
Set the default issuer to either staging or prod.
Configure client secret and access information for your DNS service account.

### OAuth2 Proxy
Configure client id and secrets for oauth2

### Nginx Ingress
You may choose to rename your Nginx Ingress class. Default is "nginx"


## Installing the DCON chart
Once your values file is configured, the rest is easy: `helm install --namespace core --name dcon repo/dcon-stack -f /my/values/file.yaml`


## Post-install
After a successful installation, you will need to do a few more things:
- Assign the A record for your Kubernetes domain to the Nginx Controller external IP
- Install the Cert Manager issuers:
  - Configure the file to use your clouddns service account and secret
  - Run: `kubectl -n core apply -f cert-issuers.yaml`
- Install the oauth proxy for Kubernetes Dashboard: `kubectl -n core apply -f dashboard-ingress.yaml`
- Test access to your dashboard, and begin deploying apps :)
